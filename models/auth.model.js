const {db} = require("../db/conexion");

const AuthModel = {
    getUserByEmail(email) {
        return new Promise((resolve, reject) => {
            db.get("SELECT * FROM usuarios WHERE email = ?", [email], (error, user) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(user);
                }
            });
        });
    },
}

module.exports = AuthModel;