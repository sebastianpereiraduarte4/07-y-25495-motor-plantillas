const {db, getAll, run, matriculaExistente, getLastId} = require("../db/conexion");
const TipoMediaModel ={

    //getAll - obtener todos los registros - filtros opcionales de búsqueda deben estar disponibles por cada campo de la tabla
    async getAll(req) {
        let baseQuery = `
        SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
        FROM media
        LEFT JOIN tipoMedia ON media.idTipomedia = tipoMedia.id
        LEFT JOIN integrantes ON media.idIntegrante = integrantes.matricula
        WHERE media.activo = 1`;

        let queryParams = [];

        if (req.query['s']) {
            if (req.query['s']['integrante']) {
                baseQuery += ` AND integrantes.matricula LIKE?`;
                queryParams.push(`%${req.query['s']['integrante']}%`);
            }

            if (req.query['s']['tipoMedia']) {
                baseQuery += ` AND tipoMedia.id LIKE?`;
                queryParams.push(`%${req.query['s']['tipoMedia']}%`);
            }

            if (req.query['s']['titulo']) {
                baseQuery += ` AND media.titulo LIKE?`;
                queryParams.push(`%${req.query['s']['titulo']}%`);
            }
        }


        return new Promise((resolve, reject) => {
            db.all(baseQuery, queryParams, (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },

    //getByid - obtener un registro por id
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM media WHERE id = ?`, [id], (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },


    //obtener un registro por un campo arbitrario
    getByField(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    if (row) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            });
        });
    },


    //crear registro
    async create(req, srcPath) {
        return new Promise((resolve, reject) => {
            db.run("insert into media (url, src, alt, idTipomedia, idIntegrante, activo) values (?, ?, ?, ?, ?, ?)", [req.url, srcPath, req.alt, req.idTipomedia, req.idIntegrante, req.activo,], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },
    //actualizar registro
    update(req, id, srcPath) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE media SET url = ?, src = ?, alt = ?, idTipomedia = ?, idIntegrante = ? WHERE id = ?", [req.url, srcPath, req.alt, req.idTipomedia, req.idIntegrante, id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    getMediaByMatricula(idIntegrante) {
        return new Promise((resolve, reject) => {
            db.all(`SELECT * FROM media WHERE activo = 1 AND idIntegrante = ? ORDER BY id`, [idIntegrante], (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },

    //borrar registro
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE media SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

}

module.exports = TipoMediaModel;