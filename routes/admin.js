const express = require('express');
//const {getAll, run, matriculaExistente, getLastId} = require("../db/conexion");
const router = express.Router();
//const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/assets/" });
const fileUpload = upload.single("src");
const session = require("express-session");
const AuthMiddleware = require('../middlewares/auth.middleware');
const AdminIntegrantesController = require("../controllers/admin/integrantes.controller");
const AdminTipoMediaController = require("../controllers/admin/tipo_media.controller");
const AdminMediaController = require("../controllers/admin/media.controller");
const InicioControllerAdmin = require('../controllers/admin/inicio.controller');


//prefijo /admin
router.get("/", (req, res) => {
    res.render("admin/index");
});

router.use(session({
    name: "session",
    secret: "clave-aleatoria-y-secreta",
    resave: false,
    saveUninitialized: true
}));

router.use((req, res, next) => {
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});

router.use(AuthMiddleware.validateAuth);

router.get("/",  InicioControllerAdmin.index);

///////////////////////////////////////////////////////////////////////////////////////
//INTEGRANTES

router.get("/integrantes/listar", AdminIntegrantesController.index);

router.get("/integrantes/crear", AdminIntegrantesController.crearForm);

router.post("/integrantes/create", AdminIntegrantesController.store);

router.post('/integrantes/delete/:matricula', AdminIntegrantesController.destroy);

router.get('/integrantes/editForm/:matricula', AdminIntegrantesController.edit);

router.post('/integrantes/update/:matricula', AdminIntegrantesController.update);


///////////////////////////////////////////////////////////////////////////////////////////
//MEDIA
router.get("/media/listar", AdminMediaController.index);

router.get("/media/crear", AdminMediaController.crearForm);

router.post("/media/create", fileUpload, AdminMediaController.store);

router.get('/media/editForm/:id', AdminMediaController.edit);

router.post('/media/update/:id',fileUpload, AdminMediaController.update);

router.post('/media/delete/:id', AdminMediaController.destroy);


///////////////////////////////////////////////////////////////////////
//TIPO DE MEDIA
router.get("/tipo_media/listar", AdminTipoMediaController.index);

router.get("/tipo_media/crear", AdminTipoMediaController.crearForm);

router.post("/tipo_media/create",fileUpload, AdminTipoMediaController.store);

router.get('/tipo_media/editForm/:id', AdminTipoMediaController.edit);

router.post('/tipo_media/update/:id',fileUpload, AdminTipoMediaController.update);

router.post('/tipo_media/delete/:id', AdminTipoMediaController.destroy);


module.exports = router;