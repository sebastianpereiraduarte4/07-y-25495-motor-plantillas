const express = require("express");
const router = express.Router();
require("dotenv").config();
//const dbSqlite = require("../db/conexion");
//const routerAdmin = require("../routes/admin");
const {getAll} = require("../../db/conexion");


const WorldCloudController = {
    worldCloud: async function (req, res) {
        const rows = await getAll("select * from integrantes",);
        res.render("word_cloud", {
            integrantes:rows,
            materia:process.env.MATERIA,
            alumno:process.env.ALUMNO,
            repositorio:process.env.ENLACE_REPOSITORIO
        });
    }
};


module.exports = WorldCloudController;