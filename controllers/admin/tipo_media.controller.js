
const tipoMediaStoreSchema = require("../../validators/tipo_media/create");
const tipoMediaUpdateSchema = require("../../validators/tipo_media/edit");
const TipoMediaModel = require("../../models/tipo_media.model");
const MediaModel = require("../../models/media.model");
const {getAll, run, getLastId} = require("../../db/conexion");

const TipoMediaController = {

    index: async function (req, res) {
        try {
            const tipoMedia = await TipoMediaModel.getAll(req);
            res.render("admin/tipo_media/index", {
                tipoMedia: tipoMedia
            });
        } catch (error) {
            res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al obtener los tipos de media!'));
        }
    },

    crearForm: function(req, res) {
        res.render("admin/tipo_media/crearForm", {
        });
    },

    store: async function (req, res) {
        const {error} = tipoMediaStoreSchema.validate(req.body);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent(errorMessages.join(';'))}&nombre=${encodeURIComponent(req.body.nombre)}`);
        } else {
            try {
                await TipoMediaModel.create(req.body);
                res.redirect(`/admin/tipo_media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },



    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            const tipoMedia = await TipoMediaModel.getById(id);
            res.render("admin/tipo_media/editForm", {
                tipoMedia: tipoMedia
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al obtener el tipo media!'));
        }
    },


    update: async function (req, res) {
        const {error} = tipoMediaUpdateSchema.validate(req.body);
        const id = parseInt(req.params.id);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/tipo_media/editForm/${id}?error=${encodeURIComponent(errorMessages.join(';'))}`);
        } else {
            try {
                await TipoMediaModel.update(req.body, id);
                res.redirect("/admin/tipo_media/listar?success=" + encodeURIComponent('¡Tipo media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al actualizar el tipo media!'));
            }
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        const idAsociado = await MediaModel.getByField('media', 'idTipomedia', id);

        if (idAsociado) {
            res.redirect(`/admin/tipo_media/listar?error=${encodeURIComponent('¡No se puede eliminar el registro porque está siendo utilizado en la tabla media!')}`);
        } else {
            try {
                await TipoMediaModel.delete(id);
                res.redirect("/admin/tipo_media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },


};

module.exports = TipoMediaController;