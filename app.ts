import express from 'express'
const bodyParser= require("body-parser")
const hbs = require("hbs");
require("dotenv").config();

//importamos el archivos de rutas
const session = require("express-session");
const router = require("./routes/public.js");
const routerAdmin = require("./routes/admin.js");
const routerAuth = require('./routes/auth');




const app = express();
app.use(session({
    name: "session",
    secret: "clave-aleatoria-y-secreta",
    resave: false,
    saveUninitialized: false
}));

app.use((req, res, next) => {
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

app.use("/", router);
app.use("/admin", routerAdmin);
app.use("/auth", routerAuth);

hbs.registerHelper('eq', function(a:any, b:any) {
    return a === b;
});


//control 404 si no encuentra ninguna ruta de las anteriores agregadas
//poner solo en la raiz y no en ningun ruta en particular
app.use((req, res) => {
    res.status(404).render('./partials/error_404');
});


const puerto = process.env.PORT
app.listen(puerto, () => {
         console.log("El servidor se está ejecutando en http://localhost:" + puerto);
});
