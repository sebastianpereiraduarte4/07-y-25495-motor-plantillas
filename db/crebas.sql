CREATE TABLE IF NOT EXISTS "integrantes" (
    "id" INTEGER NOT NULL UNIQUE,
	"matricula" TEXT NOT NULL UNIQUE,
	"nombre" TEXT NOT NULL,
	"apellido" TEXT NOT NULL,
	"activo" BOOLEAN NOT NULL,
	PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "media" (
"id" INTEGER NOT NULL UNIQUE,
"src" TEXT,
"url" TEXT,
"alt" TEXT NOT NULL,
"idIntegrante" TEXT,
"idTipomedia" TEXT,
"activo" BOOLEAN NOT NULL,
PRIMARY KEY("id")
FOREIGN KEY ("idIntegrante") REFERENCES "integrantes"("id")
ON UPDATE NO ACTION ON DELETE NO ACTION
FOREIGN KEY ("idTipomedia") REFERENCES "tipoMedia"("id")
ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "tipoMedia" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"activo" BOOLEAN NOT NULL,
	PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "usuarios" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "email" TEXT NOT NULL,
    "contrasenha" TEXT NOT NULL,
    "super_usuario" INTEGER NOT NULL,
    "matricula" TEXT NOT NULL,
    FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
    ON UPDATE NO ACTION ON DELETE NO ACTION
);
