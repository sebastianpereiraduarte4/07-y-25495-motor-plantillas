const  sqlite3= require("sqlite3").verbose();

const db= new sqlite3.Database("./db/integrantes.sqlite",
    sqlite3.OPEN_READWRITE,
    (error) => {
        if(error)
            console.log("ocurrio un error", error.message);
        else{
            console.log("conexion exitosa");
            db.run("select * from integrantes");
        }
    }
);

async function getAll(query, params) {
    return new Promise((resolve, reject) => {
        db.all(query, params, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

async function run(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        })
    })
}


async function matriculaExistente(matricula) {
    const result = await getAll("SELECT 1 FROM integrantes WHERE matricula = ?", [matricula]);
    return result.length > 0;
}

async function getLastId(tablaNombre) {
    const result = await getAll(`SELECT MAX(id) as maxId FROM ${tablaNombre}`);
    return result[0].maxId;
}


module.exports = {db, getAll, getLastId, matriculaExistente, run};